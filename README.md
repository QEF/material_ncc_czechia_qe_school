# Material for the MaX NCC Czechia QE-2024 online school

This repository collects tutorials, slides, and other material used during for 
the NCC Czechia course: _MaX School: Molecular Modelling with Quantum ESPRESSO_ held online from 19 to 21
June 2024. 

## Structure of the hands-on excercises

For each day, the respective hands-on exercises are located in
`Day-X/` folder, where `X` is the day number. Each `Day-X/` folder
contains the `README.md` file, which is a very short description of
exercises, and PDF file(s) that contain(s) slides, which
describe the hands-on exercises in more detail.

For each day, the exercises are located within the `Day-X/` directory
and are numbered sequentially. For example, here is the structure of
the `Day-1/` folder:

```
Day-1/
  README.md
  example_0-1-2.pdf
  handson-day1-afternoon.pdf
  daily-update.sh
  example0.installation/
  example1.benzene/
  example2.graphene/
  example2.1.Si/
  example2.2.Al/
  example2.3.Fe/
```

## The latest version of hands-on exercises

To get the latest version of the hands-on exercises from the git repo at [https://gitlab.com/QEF/material_ncc_czechia_qe_school](https://gitlab.com/QEF/material_ncc_czechia_qe_school), execute:

```
./daily-update.sh
```

