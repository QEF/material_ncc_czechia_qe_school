\documentclass[landscape]{foils}
\usepackage[pdftex]{color}
\usepackage[pdftex]{graphicx}
\usepackage{eso-pic}
\usepackage[top=2cm, bottom=2cm, outer=0cm, inner=0cm]{geometry}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[pdftex,colorlinks=true]{hyperref}

\input{aliases}

\begin{document}
\AddToShipoutPictureBG*{
  \AtPageUpperLeft{%
    \raisebox{-\height}{%
      \includegraphics[width=\paperwidth]{figs/qe-max-2024.png}
    }
  }
}
\blue
~\\
\vspace*{4cm}
\MyLogo{~}
\vspace{3em}
\begin{center}
  {\burgundy\LARGE\bf  Hands-on session -- Day-2 }\\[2em]
  {\burgundy\LARGE (Structural relaxations + NEB; PWTK)}
  ~\\[1.5em]
%    \large  Anton Kokalj, \\
%  Matic Pober\v{z}nik, Unmesh Mondal, Felice Conte, Tao Jiang
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\Head{Hands-on session: Day-2 morning}
\MyLogo{\burgundy MaX School: Materials and Molecular Modelling with Quantum ESPRESSO, 19--21 June 2024}
\leftheader{\hspace{-0.8cm}\includegraphics[width=4.5cm]{figs/max-logo.png}}
\rightheader{\hspace{-0.8cm}\includegraphics[width=4.5cm]{figs/QE.png}}
Topics of Day-2 hands-on session:
\begin{itemize}
\item Structural optimizations:
  \begin{itemize}
  \item relaxations -- atomic positions only (\file{example1.relax/})
  \item variable-cell relaxations (\file{example2.vc-relax/})
  \end{itemize}
\item NEB method: saddle points of elementary chemical
  reactions (\file{example3.neb/})
\item Automating the workflow with PWTK
  (\file{example4.pwtk/})\\
\end{itemize}

%\vspace{-0.5em}
To get the latest version of the exercises, move to \file{Day-2/} directory and execute:\\[0.5em]
\exec{./daily-update.sh}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\head{ Some hpc connection utilities}
\rightheader{}
\leftheader{}
Several utility commands have been implemented specially for the
QE-MaX-2024 school to aid at submitting jobs to HPC cluster(s). These are:
{\small
\begin{itemize}
\item \cmd{\bf remote\_squeue} -- this checks the status of the job in the queuing
  system on the ``hpc'' system.  
\item \cmd{\bf hpc} -- this makes \cmd{ssh} to ``hpc'' login node,
  such that the user will be located in the same directory as used
  locally
\vspace{0.5em}
  
\item \cmd{\bf rsync\_to\_hpc} -- copies specified files to the
  ``hpc'' cluster to the same directory as is currently
  used locally.
  
\item \cmd{\bf rsync\_from\_hpc} -- download the specified file from
  the ``hpc'' cluster from the same directory as is
  currently used locally.
\end{itemize}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\head{1. How to perform structural optimization: graphane}
\rightheader{}
\rightfooter{Example: \file{Day-2/example1.relax/}}

\parbox{17cm}{
  \begin{itemize}
  \item Move to \file{Day-3/example1.relax/} directory.\\[0.5em]
    Graphane is like graphene, with an H atom bound to each C atom in
    {\em trans} configuration. You need to optimize atomic positions,
    i.e., find the minimum-energy structure (zero forces).
  \end{itemize}
} \hskip 1cm
\parbox{8cm}{ \includegraphics[width=8cm]{figs/graphane.pdf}}

As a reminder, the honeycomb lattice is of hexagonal type, with the lattice
parameter $a$. In periodic representation there is a second parameter $c$. The lattice
vectors are:\\
%
\centerline{$\displaystyle
  {\bf a}_1=(a,0,0), \quad {\bf a_2}=(-{a\over 2},{a\sqrt{3}\over 2},0),
  \quad {\bf a}_3=(0,0,c)$}
%

\vspace*{-3mm} % To enforce single pages

\begin{itemize}
\item File \file{pw.graphane.relax.in} is a modified version of\\
  \file{pw.graphene1x1.scf.in} with:
  \begin{itemize}
  \item \var{calculation='relax'} for structural optimization and a
    new namelist \nml{\&IONS} with variable \var{upscale=100.0}
  \item \var{ntyp=2} (2 types of atoms), \var{nat=4}
    (4 atoms in the cell)
  \item \card{ATOMIC\_SPECIES} card with 2 species of atoms and
    pseudopotentials
  \item \card{ATOMIC\_POSITION} card with 4 initial positions
    (C--H distance $\sim 1$~\AA)
  \end{itemize}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\head{1. Structural optimization: graphane (II)}
\begin{itemize}
\item Run the structural optimization, i.e.:\\[0.5em]
  \exec{pw.x -in pw.graphane.relax.in > pw.graphane.relax.out \&}
\item When calculation finishes, analyze the output: it consists of
   several SCF steps, followed by calculation of forces and generation
   of new atomic positions.
\item To visualize the evolution of the structure during structural
   optimization, execute:\\[1em]
   \exec{xcrysden --pwo pw.graphane.relax.out}
\end{itemize}
\parbox{15cm}{Relaxed structure of graphane exhibits ``buckling''.}
\hskip 0.5cm \parbox{8cm}{
  \includegraphics[width=8cm]{figs/graphane2.pdf}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\head{1. Supercell and structural optimization: graphene-oxide}

\parbox{15cm}{
  \begin{itemize}
  \item The first stage of graphene oxidation is the formation of an
    epoxy bridge. Let us add an O atom on a $(3\times3)$ supercell of
    graphene
  \end{itemize}
}\hskip 2cm\parbox{7cm}{
  \begin{flushright}
    \includegraphics[width=7cm]{figs/graphene3x3-O.pdf}    
  \end{flushright}
}

This example consists of a structure with an O atom on a
graphene--$(3\times3)$ supercell structure and run a relaxation
calculation.
    
\vspace{-1em}
\begin{itemize}
\item The file \file{pw.graphene3x3-O.relax.in} has been prepared to you, the relevant feature of this file are:
  \begin{itemize}  
  \item \var{calculation='relax'} for structural optimization and a new
    namelist \card{\&IONS}
  \item \var{ntyp=2} (2 types of atoms), \var{nat=19}
    (19 atoms in the cell)
  \item \card{ATOMIC\_SPECIES} card with 2 species of atoms and
    pseudopotentials
  \item \card{ATOMIC\_POSITION} card with 19 initial positions
    (C--O distance $\sim 1.5$~\AA)
  \end{itemize}
\item Run the structural optimization and analyze the output
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\head{2. How to perform variable-cell relaxation: hcp-Zinc}
\rightfooter{Example: \file{Day-2/example2.vc-relax/}}
%
Zinc displays an hcp (hexagonal-closed-packed) crystal structure, hence
it has two lattice parameters $a$ and $c$. The unit-cell lattice
vectors are:\\
%
\centerline{$\displaystyle
  {\bf a}_1=(a,0,0), \quad {\bf a_2}=(-{a\over 2},{a\sqrt{3}\over 2},0),
  \quad {\bf a}_3=(0,0,c)$}
%

This lattice can be described as \var{ibrav=4}, \var{celldm(1)=$a$}, \var{celldm(3)=$c/a$},
as in the file \file{pw.Zn.vc-relax.in}

\vspace{2.5em}
This can be done by using the {\em variable-cell relaxation}. (See
also \file{README.md}).
Notice:
\begin{itemize}
  \item \var{calculation = 'vc-relax'}
  \item \nml{\&IONS} and \nml{\&CELL} namelists are specified after
    the \nml{\&ELECTRONS}
\end{itemize}

\vspace{5.5em}
To run the calculation, execute:\\[0.5em]
\exec{pw.x -in pw.Zn.vc-relax.in > pw.Zn.vc-relax.out}\\[0.5em]
Inspect the output (file: \file{pw.Zn.vc-relax.out}) and notice that:
\vspace{-1em}
\begin{itemize}
  \item several scf steps are performed, forces (zero by symmetry) 
    and stresses computed
    \vspace{-1em}
  \item the energy and the stress decrease as the minimum is approached
    \vspace{-1em}
  \item a final scf step is performed with plane waves computed for
    the final cell
    \vspace{-1em}
  \item the final cell is printed after the last \texttt{CELL\_PARAMETERS}
    card
  \end{itemize}
Check that the optimized parameters are different from the ones specified initially.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\head{2. Variable-cell relaxation (II): molecular crystal of Urea}

While in the preceding example, the forces were zero by symmetry, in
this example (\file{pw.urea.vc-relax.in}) both unit-cell and atomic
positions are optimized by utilizing the {\em variable-cell
  relaxation} (\var{calculation = 'vc-relax'}).  Beware that it is
computationally heavier than the \file{pw.Zn.vc-relax.in} example.
\begin{center}
  \includegraphics[width=0.4\textwidth]{figs/urea.png}
\end{center}
See the instructions in \file{README.md} for how to run this
calculation remotely.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\head{3. NEB method: saddle points of elementary chemical reactions}
\rightfooter{Example: \file{Day-2/example3.neb/}}
%
Saddle points on the {\em Potential Energy Surface (PES)}, which
correspond to {\em Transition States (TS)} of chemical reactions, can
be found by means of the {\em Nudged Elastic Band (NEB)} method.

In this example we will analyze a simple H$_2$ dissociation on the
Al(100) surface, where we will use chemical intuition to construct a
better initial reaction path. This is achieved by using the
\card{INTERMEDIATE\_IMAGE} card, in which a rough guess for the
transition state is provided. You can visualize the path by using
\prog{xcrysden}:

\exec{ xcrysden --pwi neb.H2-diss.Al100-2x1-2L.in}

By useing the \card{INTERMEDIATE\_IMAGE} two H atoms start to move
away from each other when closer to the surface, which is much more
similar to the actual transition state.

Additionally, we will be using climbing-image NEB (CI-NEB), however
initially we do not know which image is the {\em climbing-image},
because its ID can change from iteration to iteration, depending on
how far from convergence we are.
\vspace{2.5em}

Thus we will perform the NEB calculation in two steps,  first perform a plain NEB (\var{CI\_scheme = 'no-CI'}) in
order to stabilize the pathway and only once the pathway is stabilized we will
switch on the CI-NEB with \var{CI\_scheme~=~'auto'}. We will use a
PWTK script to manage these two calculations.

{\bf{Steps:}}
\begin{itemize}
\item{Read \file{neb.H2-diss.Al100-2x1-2L.in} and try to understand
    it.
    This file will serve as the main input for the \prog{pwtk} script.}
\item{Read the \file{neb.pwtk} script and try to understand it. }
\item{Beware that this example takes about 20--30 min on a
    laptop. Hence, run the calculation remotely, i.e., submit it to
    the HPC cluster, i.e.:\\
    \exec{hpc}}\\
    \exec{pwtk --slurm neb.pwtk}
\item To download the calculated output files, use:\\
  \exec{rsync\_from\_hpc '*.out'}\\[0.5em]  
  Give the remote computer some time to make the calculation. To
  download other data files that were produced by \prog{neb.x}, use:\\[0.5em]
  \exec{rsync\_from\_hpc .}
\item{Once the calculation converges analyze \file{neb.noCI.out} and
    \file{neb.auto.out} and check the number of steps and the reaction
    energy barrier.
    The reaction energy graph can be plotted by:\\
    \exec{gnuplot H2-diss.Al100-2x1-2L.gp} \\[0.5em]
    and the corresponding path visulized by: \\
    \exec{xcrysden --axsf H2-diss.Al100-2x1-2L.axsf } }
\end{itemize}

\clearpage

\vspace*{1em}
The resulting PES along the reaction coordinate should look like this.
\vspace*{1em}
\begin{figure}
  \centering
    \includegraphics[width=14.cm]{figs/NEB-H2onAl100-results.pdf}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\head{4. PWTK: a short tutorial}
\rightheader{\includegraphics[width=2.5cm]{figs/pwtk.png}}
\rightfooter{Example: \file{Day-2/example4.pwtk/}}

PWTK is a Tcl-based scripting interface for Quantum ESPRESSO. It aims
at providing a flexible and productive framework.

\begin{itemize}
\item PWTK web-site is:\\
  \file{http://pwtk.quantum-espresso.org/} ~or~ \file{http://pwtk.ijs.si/}
\item PWTK documentation is available at:\\
  \href{http://pwtk.ijs.si/documentation.html}{http://pwtk.ijs.si/documentation.html}\\
\end{itemize}

\vspace*{7.0em}

\rightheader{}
Move to \file{Day-3/example4.pwtk/} directory.

We will use PWTK to analyze the bonding of the CO molecule on
the Rh(100) surface. To this end we will perform the following tasks:
\begin{itemize}
  \item relaxation of CO/Rh(100)-c(2$\times$2)
    \vspace{0.5em}
  \item a charge-density difference (DIFDEN) calculation between CO and Rh(100)-c(2$\times$2)
    \vspace{0.5em}
  \item a projected density of states (PDOS) calculation of CO/Rh(100)-c(2$\times$2) with a denser k-mesh
    \vspace{0.5em}
  \item an integrated local density of states (ILDOS) calculation of CO/Rh(100)-c(2$\times$2)
\end{itemize}
The steps that you need to perform are in the `README.md` of the example directory.
\begin{center}
  \includegraphics[width=0.9\textwidth]{figs/co-rh100-non-shifted-ldos.png}
\end{center}

\begin{center}
  \includegraphics[width=0.7\textwidth]{figs/ildos-pwtk.png}
\end{center}

 
\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
