# Day-2 :
---------

### Topics of Day-2 morning hands-on session

- Structure optimizations
- NEB: transition states of elementary chemical reactions
- Automating the workflow with PWTK

---

**Exercise 1:** structural optimization (`calculation = 'relax'`);
	            atomic positions only

    cd example1.relax/


**Exercise 2:** structural optimization (`calculation = 'vc-relax'`);
                atomic positions and unitcell

    cd example2.vc-relax/


**Exercise 3:** transition states of elementary chemical reactions

    cd example3.neb/


**Exercise 4:** automating the workflow with PWTK

    cd example4.pwtk/


### Topics of Day-2 afternoon hands-on session 

**Exercise 1:** Calculation of CoO projected density of states with and without Hubbard correction

       `cd Functionals/exercise1.DFT+U/` 

**Exercise 2:** Calculation of U Hubbard parameter with `hp.x` 

      `cd Functionals/exercise2.DFT+U/`

**Exercise 3:** Calculation of gap of Si fcc with PBE0

    `cd Functionals/exercise3-PBE0/` 

**Exercise 4:** Calculation of graphite interplanar distance with vdw-DF functional

   `cd  Functionals/exercise4-vdW/`   
