#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=1
#SBATCH --time=00:10:00
#SBATCH --account=dd-23-116
#SBATCH --partition=qcpu
#SBATCH --reservation=dd-23-116_2024-06-20T09:00:00_2024-06-20T17:00:00_60_qcpu

module purge
module load intel-compilers/2022.1.0
module load impi/2021.6.0-intel-compilers-2022.1.0
module load imkl/2022.1.0
module load imkl-FFTW/2022.1.0-iimpi-2022a
module load libxc/5.2.3-intel-compilers-2022.1.0

export ESPRESSO_PSEUDO=/mnt/proj3/dd-23-116/pseudo
export OMP_NUM_THREADS=1
export OMP_PLACES=cores
export OMP_PROC_BIND=close


export PW=/mnt/proj3/dd-23-116/qe-7.3.1/build/bin/pw.x
export PROJWF=/mnt/proj3/dd-23-116/qe-7.3.1/build/bin/projwfc.x 

srun --cpu-bind=cores --cpus-per-task=$SLURM_CPUS_PER_TASK -n 8 $PW  -i CoO.scf.in   > CoO.scf.out 
srun --cpu-bind=cores --cpus-per-task=$SLURM_CPUS_PER_TASK -n 8 $PW  -i CoO.nscf.in   > CoO.nscf.out
srun --cpu-bind=cores --cpus-per-task=$SLURM_CPUS_PER_TASK -n 8 $PROJWF  -i CoO.projwfc.in    > CoO.proj.out
