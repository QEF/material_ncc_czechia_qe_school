# PURPOSE OF THE EXERCISE:
## How to perform a variable-cell structural optimization (calculation = "vc-relax").
------------------------------------------------------

There are two examples of variable cell relaxation: a hcp Zn-bulk
structure and molecular crystal of urea.


### Example-1: hcp Zn-bulk
--------------------------
  
Steps to perform:

1. Read the pw.Zn.vc-relax.in input file and try to understand
   it. Notice the new &IONS and &CELL namelists. Beware that "IONS" is
   a jargon in pseudo-potential context for atoms (can you figure out
   why?)

   
2. To run the example, execute:

       pw.x -in pw.Zn.vc-relax.in > pw.Zn.vc-relax.out &


3. When calculation finishes, analyze the output: it consists of
   several SCF steps, followed by calculation of forces and stress
   tensor, and generation of new cell parameters and atomic positions.


4. To visualize the evolution of the structure during structural
   optimization, execute:

       xcrysden --pwo pw.Zn.vc-relax.out

### Example-2: molecular crystal of urea on the HPC
----------------------------------------

Example-2 is analogous to example-1, just the structure is different.
**Beware** that it is **computationally heavier** than the example-1. To run
this example we will utilize the HPC:

To login to the HPC in the mirror directory, you can use the command: 

    hpc

that was prepared in the virtual machine. Note that you can also `ssh`
to the hpc and `cd` to the appropriate directory.

To submit the calculation an submission script, `run_urea_vc-relax.sh`
was prepared for you. To submit the calculation to the HPC cluster,
use:

	 sbatch run_urea_vc-relax.sh

You can check the status of the calculation, while logged in to the HPC as:  
	 
	 squeue -u $USER

or from the VM using the command: 
     
	 hpc_squeue 

Once the calculation is done you can visualize the structure with
`xcrysden`. To do that you will need to download the calculated output
file `pw.urea.vc-relax.out` to your virtual machine. You can do that
via:

       rsync_from_hpc '*.out'

But wait some time before doing that; give the remote computer some
time to make the calculation.
