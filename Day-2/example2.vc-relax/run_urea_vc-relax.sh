#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=1
#SBATCH --time=00:20:00
#SBATCH --account=dd-23-116
#SBATCH --partition=qcpu
#SBATCH --reservation=dd-23-116_2024-06-20T09:00:00_2024-06-20T17:00:00_60_qcpu
module purge 
module load QuantumESPRESSO 

mpirun -np 8 pw.x < pw.urea.vc-relax.in > pw.urea.vc-relax.out

