# PURPOSE OF THE EXERCISE:
## To show a real (more elaborate) PWTK example of how to glue together various calculations
--------------------------------------------------------------------

The subject of this example is the analysis of bonding of the CO
molecule on the Rh(100) surface by means of various techniques, such
as charge-density difference (DIFDEN), density-of-states projected
(PDOS) to atomic orbitals, and ILDOS (integrated-local density of states).

This example is run with the `bonding-analysis-CORh.pwtk` script, open the script with an editor, i.e.,: 
        
	emacs bonding-analysis-CORh.pwtk

In it the workflow is defined, which consists of a series of tasks. First, a relaxation is perfomed, 
where 

  1. the CO@Rh(100)-c(2×2) structure from the `CO-Rh100-c2x2-2L.xsf` file is loaded using the `CELL_PARAMETERS_and_ATOMIC_POSITIONS_fromXSF` command, i.e.:
       
	        CELL_PARAMETERS_and_ATOMIC_POSITIONS_fromXSF CO-Rh100-c2x2-2L.xsf
  
  2. the bottom layer of Rh(100) is fixed and the `relax` pw.x calculation is performed: 
       
	        fixAtomsLast 2
		    runPW relax.CO-Rh100-c2x2-2L.in

Once the relaxation is done, the optimized structure from the pw.x output file is loaded with `ATOMIC_POSITIONS_fromPWO`:
       
	    ATOMIC_POSITIONS_fromPWO relax.CO-Rh100-c2x2-2L.out

In the second part, the DIFDEN calculation is performed, where the `pp.x` input data is first defined: 
      
	   INPUTPP { ... }
       PLOT { ... }

The fragments for which the charge density will be calculated are defined in the `DIFDEN` namelist, where the specifics for a particular segment can be specified with

     difden_segmentSpec 2 { .... }

The `DIFDEN` calculation is then requested with: 
     
	 difden_run difden.CO-Rh100-c2x2-2L.out

This calculation is followed by a `projwfc.x` calculation with the command `pdos_run`, whereas the `sumldos` and `ldos_plot` utilities are there to sum and plot the projected density of states for a particular atom. 

To run the example launch:
        
	pwtk bonding-analysis-CORh.pwtk >& bonding-analysis-CORh.log &

Once the calculation is finished, we can plot the charge density difference, 
which was written into `difden.CO-Rh100-c2x2-2L.xsf`, i.e.:
    
	xcrysden -r 2 --xsf difden.CO-Rh100-c2x2-2L.xsf
	
and plot the 3D isosurface. 

Once the calculation is finished, open the `non-shifted-ldos.png` image: 
    
	eom non-shifted-ldos.png
	
and select a few energy ranges and edit the `ildos-CORh.pwtk` script,
and input the energy ranges, i.e,
   
    emacs ildos-CORh.pwtk

and run the script as: 
    
	pwtk ildos-CORh.pwtk >& ildos-CORh.log &

Once the script is finished, you can visualize an ILDOS XSF: 
 
    xcrysden -r 2 --xsf ildos_-6.00_-5.50.CO-Rh100-c2x2-2L.xsf

Once you are satisfied with the visual representation, save the xcrysden script with `File -> Save Current State` 

Finally, edit the `visualize-ildos-CORh.pwtk` script and insert the same energy ranges you specified in `ildos-CORh.pwtk` script:

    emacs visualize-ildos-CORh.pwtk

Once done, run the script as: 

    pwtk visualize-ildos-CORh.pwtk

