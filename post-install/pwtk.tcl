# ------------------------------------------------------------------------ 
# pwtk.tcl
#
# This is the main configuration file for PWTK. It contains basic
# configuration, such as the location of Quantum ESPRESSO executables
# and how to run them...
# ------------------------------------------------------------------------


#
# HOW TO RUN QUANTUM ESPRESSO (QE) EXECUTABLES
# (e.g.:  prefix bin_dir/pw.x postfix -in INPUT > OUTPUT)

# this snippet auto-detects the number of available processors
try {
    set np [exec nproc]
    while { [catch {exec mpirun -n $np echo yes}] && $np > 1 } {
        set np [expr { $np > 2 ? $np / 2 : 1 }]
    }
    if { $np > 1 } {
        prefix mpirun -n $np
    }
}


# directory containing the QE executables (bin_dir);
bin_dir /opt/qe-7.3.1


# directory containing pseudopotentials (pseudo_dir);
#
# N.B. make sure that 'pseudo_dir' exists with 'file mkdir'
file mkdir [pseudo_dir $env(HOME)/pw/pseudo]

## disable auto-downloading of pseudopotentials from the internet
#pseudo_auto_download off


# directories where the scratch files are written (outdir)
# (i.e. outdir == outdir_prefix/outdir_postfix)

outdir_prefix   /tmp/QE
outdir_postfix  [pid]
# make sure 'outdir' exists
file mkdir [outdir]

#wfcdir_prefix  /tmp/QE
#wfcdir_postfix [pid]
