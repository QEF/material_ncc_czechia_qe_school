#!/bin/bash
#
# make a ssh configure file for the user
#
userinput() {
    echo "
Please input your HPC '$1' that you received by email:"
    read reply
    
    echo -n "
The $1 is \"$reply\". Is this correct [y|n] : "
    read answer

    if test "$answer" != "y"; then
	#echo "The reply $reply was wrong, exiting ..."
	#exit 1
        echo "Retrying ..."
        userinput $1
    fi
} 

echo "
CAREFULLY READ THE INSTRUCTIONS BELOW AND ANSWER QUESTIONS."


#
# HPC username
#

userinput username
user=$reply
host=barbora.it4i.cz

   
cat >> $HOME/.ssh/config <<EOF 
Host hpc
 HostName $host 
 User $user
EOF

 
