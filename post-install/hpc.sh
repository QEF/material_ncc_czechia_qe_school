# 
# This file contains a few convenience shell functions to facilitate
# the communication with the HPC cluster
#

HPC_HOST=${HPC_HOST:-hpc}

hpc_squeue () {
    # Purpose: execute "squeue -u $USER" on the HPC cluster
    ssh -x -n -f $HPC_HOST "echo; squeue -u \$USER"
}


hpc() {
    # Purpose: log to the HPC cluster and cd into CWD    
    HERE="~${PWD#$HOME}"
    ssh -t $HPC_HOST "cd $HERE; bash";
}

hpc_mkcwd() {
    # Purpose: create a mirror of the CWD on the HPC cluster
    HERE="~${PWD#$HOME}"
    if test x$HERE != x; then
        ssh $HPC_HOST "mkdir -p $HERE"
    fi
}

rsync_to_hpc() {
    # Usage: rsync_to_hpc files	
    # Purpose: rsync files to the CWD on the HPC cluster
    hpc_mkcwd   
    HERE="~${PWD#$HOME}"
    rsync -avu $@ $HPC_HOST:$HERE
}
scp_to_hpc() {
    # Usage: scp_to_hpc files
    # Purpose: scp files to the CWD on the HPC cluster
    hpc_mkcwd
    HERE="~${PWD#$HOME}"
    scp $@ $HPC_HOST:$HERE
}

rsync_from_hpc() {
    # Usage: rsync_from_hpc file
    # Purpose: scf files from the CWD on the HPC cluster
    HERE="~${PWD#$HOME}"
    rsync -avu $HPC_HOST:$HERE/$1 .
}
scp_from_hpc() {
    # Usage: scp_from_hpc file
    # Purpose: scf files from the CWD on the HPC cluster
    HERE="~${PWD#$HOME}"    
    scp $HPC_HOST:$HERE/$1 .
}


# workaround (VM bug-fix)
export PATH=/opt/pwtk-3.1:$PATH
