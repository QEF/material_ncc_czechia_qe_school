# Day-1 :
---------

## Topics of Day-1 hands-on sessions

- Installation/compilation of Quantum ESPRESSO (QE)
- SCF calculation
- XCrySDen, PWgui, QE-emacs-modes
- Basic convergence tests (kinetic energy cutoff, k-points, etc.)
- How to calculate lattice parameters -- equation of state (EOS)
- Basic post-processing: band-structure, charge-density, density-of-states 
- Scripting with PWTK (basics)

### Exercises

-  **Exercise 0:** Compile Quantum ESPRESSO sources
   
       cd example0.installation/
       
-  **Exercise 1:** Benzene molecule: basic `pw.x` SCF calculation +
                   plotting of molecular orbitals using `pp.x` and
                   `xcrysden` codes
   
       cd example1.benzene/
       
-  **Exercise 2:** Graphene sheet: basic `pw.x` SCF calculation + 
                   density-of-states (DOS) and band structure (spaghetti)
                   plots using `dos.x` and `bands.x` codes
   
       cd example2.graphene/
       
-  **Excercise 2.1:** Sillicon bulk: convergence tests, EOS, DOS and, spaghetti band-structure plots
   
       cd example2.1.Si/
   
-  **Excercise 2.2:** A metallic example (Aluminum) -- how to deal with metals
   
       cd example2.2.Al/
       
-  **Excercise 2.3:** A magnetic example (Iron) -- how to deal with magnetic systems
   
       cd example2.3.Fe/
    

### The latest version of hands-on exercises

To get the latest version of the hands-on exercises from the git repo at [https://gitlab.com/QEF/material_ncc_czechia_qe_school](https://gitlab.com/QEF/material_ncc_czechia_qe_school), execute:

```
./daily-update.sh
```
