#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --time=00:10:00
#SBATCH --account=dd-23-116
#SBATCH --partition=qcpu

module purge
module load intel-compilers/2022.1.0
module load impi/2021.6.0-intel-compilers-2022.1.0
module load imkl/2022.1.0
module load imkl-FFTW/2022.1.0-iimpi-2022a
module load libxc/5.2.3-intel-compilers-2022.1.0

export OMP_NUM_THREADS=1
export OMP_PLACES=cores
export OMP_PROC_BIND=close

srun --cpu-bind=cores --cpus-per-task=$SLURM_CPUS_PER_TASK -n 1 'BIN_QE_PATH'/pp.x < pp.benzene.psi2.in > pp.benzene.psi2.out

