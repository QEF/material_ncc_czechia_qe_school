# PURPOSE OF THE EXERCISE:
## How to compile the Quantum ESPRESSO (QE) sources.
-------------------------------------------------


To untar the QE tarball, execute:

    tar zxvf qe-7.3.1-ReleasePack.tar.gz

Then follow the instructions of tutors. See the
[INSTALL.md](./INSTALL.md) file for more detailed instructions.

