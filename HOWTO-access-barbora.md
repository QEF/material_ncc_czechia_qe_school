# A few practical hints how to setup an easy access to barbora.it4i.cz

1. Upload your ssh key to [https://extranet.it4i.cz/ssp/?action=changesshkey](https://extranet.it4i.cz/ssp/?action=changesshkey)

   * You can:
     - either: generate ssh key in a virtual machine with `ssh-keygen`      
     - or: upload the public ssh key from your workstation/laptop and
       copy-paste the private and public keys to virtual-machine into
       the `~/.ssh/` directory (beware that the private key must be
       readable only by yourself, e.g., the `chmod 600` permission)

    
2. Once the ssh key is uploaded to [https://extranet.it4i.cz/ssp/?action=changesshkey](https://extranet.it4i.cz/ssp/?action=changesshkey), create the `~/.ssh/config` file, which should look like this:
     ```
     Host hpc
      HostName barbora.it4i.cz
      User dd-23-116-XXX
     ```
     where the *XXX* in the `dd-23-116-XXX` is replaced by your number (e.g., mine is 080). 
       
3. Once the `~/.ssh/config` file is setup, you can connect to barbora.it4i.cz as:
     ```
     ssh hpc
     ```

