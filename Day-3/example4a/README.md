PURPOSE OF THE EXERCISE 
How to calculate the phonon frequencies of the polar semiconductor AlAs at the Gamma point.

Steps to perform:

#1) Run the SCF ground-state calculation

srun -np 2 pw.x < pw.CnSnI3.in > pw.CnSnI3.out

#2) Run the phonon calculation at Gamma by distributing on 2 pools

srun -np 2 ph.x -nk 2 < ph.CnSnI3.in > ph.CnSnI3.out

#3) Impose the acoustic sum rule at the Gamma point

srun -np 1 dynmat.x < dynmat.CnSnI3.in > dynmat.CnSnI3.out
