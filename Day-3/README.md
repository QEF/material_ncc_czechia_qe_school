# Day-3 :
---------

### Topics of Day-3 hands-on session

- DFPT exercises (1a, 1b, 2a, 2b, 3)
- Pool and image distribution workflow

-----------

**Exercise 1:** Calculation of the phonon frequencies at the Gamma point and of the phonon dispersion of silicon.

   cd example1a/
   cd example1b/
    
**Exercise 2:** Calculation of the phonon frequencies at the Gamma point and of the phonon dispersion of the polar semiconductor AlAs.
    
    cd example2a/
    cd example2b/
    
**Exercise 3:** Calculation of the phonon dispersion of 2D hexagonal Boron Nitride.

    cd example3/

**Exercise 4:** Pool and image distribution workflow 

    cd example4a/
    cd example4b/

