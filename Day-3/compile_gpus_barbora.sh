#!/bin/bash
module purge
module load CMake/3.20.1
module load OpenMPI/4.0.7-NVHPC-21.9-CUDA-11.4.1
module load FFTW/3.3.10-NVHPC-21.9

cmake -DCMAKE_C_COMPILER=nvc -DCMAKE_Fortran_COMPILER=mpif90 \
-DQE_ENABLE_MPI_GPU_AWARE=ON -DQE_ENABLE_CUDA=ON -DQE_ENABLE_OPENACC=ON \
-DQE_FFTW_VENDOR=FFTW3 <ADD-SRC>

make -j all

